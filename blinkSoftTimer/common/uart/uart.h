#ifndef _UART_H_
#define _UART_H_
#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "esp_log.h"

      /*!< Set the number of consecutive and identical characters received by receiver which defines a UART pattern*/
#define MAX_NUMBER_UART 3

typedef void (*uart_callback_t) (uint8_t*);//(my_uart_dev_t, uint8_t*, uart_event_t);
typedef struct{
    uint8_t index;
    uint8_t uartNum;
    uint8_t txPin;
    uint8_t rxPin;
    // uint8_t bufTxSize;
    // uint8_t bufRxSize;
    // uint8_t queueSize;
    uart_callback_t callback;
} my_uart_dev_t;
#define BUF_SIZE (1024)
#define RD_BUF_SIZE (BUF_SIZE)
static QueueHandle_t uart0_queue;

static void uart_event_task(void *pvParameters);
uart_config_t config_uart(uint32_t baurRate, uart_word_length_t dataBits, uart_parity_t parityBit, uart_stop_bits_t stopBits, uart_hw_flowcontrol_t flowControl, uart_sclk_t sourceClock);
void init_uart(uart_config_t uart_config, uint8_t uartNum, uint8_t txPin, uint8_t rxPin);
void setCallback(void *cb, int uartNum);
#endif