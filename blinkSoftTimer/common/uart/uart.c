#include "uart.h"

static my_uart_dev_t uart_dev[MAX_NUMBER_UART];   

static int indexUART = 0;
static void uart_event_task(void *pvParameters)
{
    uart_event_t event;
    uint8_t* dtmp = (uint8_t*) malloc(RD_BUF_SIZE);
    for(;;) {
        //Waiting for UART event.
        int i = (int)pvParameters;
        my_uart_dev_t *uart = &uart_dev[i];
        if(xQueueReceive(uart0_queue, (void * )&event, (portTickType)portMAX_DELAY)) {
            //co tac dung vo cung to lon :)) dung cai nay moi ss duoc 
            bzero(dtmp, RD_BUF_SIZE);
            switch(event.type) {
                case UART_DATA:
                    uart_read_bytes(uart->uartNum, dtmp, event.size, portMAX_DELAY);
                    
                       
                    // printf("%d\n",(int)dtmp);
                    uart->callback(dtmp);
                    
                    break;
                case UART_FIFO_OVF:
                    // uart_flush_input(EX_UART_NUM);
                    // xQueueReset(uart0_queue);
                    break;
                //Event of UART ring buffer full
                case UART_BUFFER_FULL:
                    // uart_flush_input(EX_UART_NUM);
                    // xQueueReset(uart0_queue);
                    break;
                //Event of UART RX break detected
                case UART_BREAK:
                    break;
                //Event of UART parity check error
                case UART_PARITY_ERR:
                    break;
                //Event of UART frame error
                case UART_FRAME_ERR:
                    break;
                default:
                    break;
            }
        }
    }
    free(dtmp);
    dtmp = NULL;
    vTaskDelete(NULL);
}

uart_config_t config_uart(uint32_t baurRate, uart_word_length_t dataBits, uart_parity_t parityBit, uart_stop_bits_t stopBits, uart_hw_flowcontrol_t flowControl, uart_sclk_t sourceClock)
{
    uart_config_t uart_config = {
        .baud_rate = baurRate,
        .data_bits = dataBits,
        .parity = parityBit,
        .stop_bits = stopBits,
        .flow_ctrl = flowControl,
        .source_clk = sourceClock,
    };
    return uart_config;
}
void init_uart(uart_config_t uart_config, uint8_t uartNum, uint8_t txPin, uint8_t rxPin)
{
    my_uart_dev_t *uart = &uart_dev[indexUART];
    //uart->uartQueue = &uartQueue[indexUART];
    uart->index = indexUART++;
    uart->uartNum = uartNum;
    uart->txPin = txPin;
    uart->rxPin = rxPin;
    uart->callback = NULL;
    uart_driver_install(uart->uartNum, BUF_SIZE * 2, BUF_SIZE * 2, 20, &uart0_queue, 0);
    uart_param_config(uart->uartNum, &uart_config);
    uart_set_pin(uart->uartNum, uart->txPin, uart->rxPin, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    xTaskCreate(uart_event_task, "uart_event_task", 2048, (void*)uart->index, 12, NULL);
}
void setCallback(void *cb, int uartNum)
{
    int t_index;
    for(int i = 0; i < MAX_NUMBER_UART; i++)
    {
        if(uart_dev[i].uartNum == uartNum)
        {   
            my_uart_dev_t *uart = &uart_dev[i];
            uart->callback = cb;
            break;
        }
    }
}