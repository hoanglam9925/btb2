#include "press_time.h"

button_cb press_callback = NULL;
static void IRAM_ATTR gpio_input_handler(void* arg)  
{
    int gpio_num = (uint32_t) arg;
    int level = gpio_get_level(gpio_num);
    uint32_t rtc = xTaskGetTickCountFromISR();
    if(level == 1)
    {
        if(pressed == true)
        {
            _end_press_time = rtc;
            _tick_press = _end_press_time - _start_press_time;
            press_callback(gpio_num, _tick_press);
        }
        pressed = false;
    }
    else
    {
        pressed = true;
        _start_press_time = rtc;
    }
}

void iot_button_set_evt_cb(void *cb)
{
    press_callback = cb;
}
void press_init(gpio_num_t gpio_num)
{
    gpio_pad_select_gpio(gpio_num);
    gpio_set_direction(gpio_num, GPIO_MODE_INPUT);
    gpio_set_pull_mode(gpio_num, GPIO_PULLUP_ONLY);
    gpio_set_intr_type(gpio_num, GPIO_INTR_ANYEDGE);
    gpio_install_isr_service(0);
    gpio_isr_handler_add(gpio_num, gpio_input_handler, (void*) gpio_num); 
}