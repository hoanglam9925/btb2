#ifndef PRESS_TIME_H
#define PRESS_TIME_H
#include "esp_err.h"
#include "hal/gpio_types.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <stdio.h>
#include <esp_log.h>
#include <driver/gpio.h>

static uint32_t _start_press_time = 0;
static uint32_t _end_press_time = 0;
static uint32_t _tick_press;
static bool pressed = false;
// static button_dev_t button_dev;

typedef void (* button_cb)(uint8_t io_num, uint32_t tick_press);
void press_init(gpio_num_t gpio_num);
// void iot_button_set_evt_cb(void *cb)



#endif
