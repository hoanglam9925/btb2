/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/timers.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "uart.h"
#include "output_iot.h"
/* Can use project configuration menu (idf.py menuconfig) to choose the GPIO to blink,
   or you can edit the following line and set a number here.
*/
#define BLINK_GPIO CONFIG_BLINK_GPIO

TimerHandle_t  xTimers[1];

void vTimerCallback( TimerHandle_t xTimer )
{
    uint32_t ID;

    /* Optionally do something if the pxTimer parameter is NULL. */
    configASSERT(xTimer);

    ID = (uint32_t)pvTimerGetTimerID(xTimer);
    if(ID == 0)
    {
        output_io_toggle(2);
    }
    
}
void uartCall(uint8_t *t)
{
   int x = atoi((const char*)t);
   x = (x==0) ? 1/portTICK_RATE_MS : x/portTICK_RATE_MS;
   // int a = xTimerGetPeriod(xTimers[0]);
   // printf("%d\n",a);
   xTimerChangePeriod(xTimers[0], x, 0);
}
void app_main(void)
{
   output_io_create(2);
   uart_config_t uart_config = config_uart(115200, UART_DATA_8_BITS, UART_PARITY_DISABLE, UART_STOP_BITS_1, UART_HW_FLOWCTRL_DISABLE, UART_SCLK_APB);
   init_uart(uart_config, UART_NUM_0, 1, 3);
   setCallback(uartCall, UART_NUM_0);
   xTimers[0] = xTimerCreate("TimerBlink", pdMS_TO_TICKS(500), pdTRUE, (void *)0, vTimerCallback);
   xTimerStart(xTimers[0], 0);
}
