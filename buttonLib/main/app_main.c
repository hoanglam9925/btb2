/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "input_iot.h"
#include "output_iot.h"
#include "press_time.c"

#define BIT_EVENT_SHORT (1 << 0)
#define BIT_EVENT_NORMAL (1 << 1)
#define BIT_EVENT_LONG (1 << 2)
#define BIT_EVENT_TIMEOUT (1 << 3)
EventGroupHandle_t xEventGroup;
void test(uint8_t io_num, uint32_t tick_press)
{
   BaseType_t pxHigherPriorityTaskWoken;
   // xEventGroupSetBitsFromISR(xEventGroup, BIT_EVENT_BUTTON_PRESS, &pxHigherPriorityTaskWoken);
   uint32_t time_ms = pdTICKS_TO_MS(tick_press);
   if(time_ms <= 1000)
      xEventGroupSetBitsFromISR(xEventGroup, BIT_EVENT_SHORT, &pxHigherPriorityTaskWoken);
   else if(time_ms <= 3000)
      xEventGroupSetBitsFromISR(xEventGroup, BIT_EVENT_NORMAL, &pxHigherPriorityTaskWoken);
   else if(time_ms <= 5000)
      xEventGroupSetBitsFromISR(xEventGroup, BIT_EVENT_LONG, &pxHigherPriorityTaskWoken);
   else  
      xEventGroupSetBitsFromISR(xEventGroup, BIT_EVENT_TIMEOUT, &pxHigherPriorityTaskWoken);
}
void vTask1(void *pvParameters)
{
   for(;;)
   {
      EventBits_t uxBits = xEventGroupWaitBits(xEventGroup, BIT_EVENT_SHORT | BIT_EVENT_NORMAL | BIT_EVENT_LONG | BIT_EVENT_TIMEOUT, pdTRUE, pdFALSE, portMAX_DELAY);
      if(uxBits & BIT_EVENT_SHORT)
         printf("Short press\n");
      else if(uxBits & BIT_EVENT_NORMAL)
         printf("Normal press\n");
      else if(uxBits & BIT_EVENT_LONG)
         printf("Long press\n");
      else if(uxBits & BIT_EVENT_TIMEOUT)
         printf("Timeout\n");    
      vTaskDelay(10/portTICK_RATE_MS);
   }
}
void app_main(void)
{
   xEventGroup = xEventGroupCreate();
   output_io_create(GPIO_NUM_2);
   iot_button_set_evt_cb(test);
   press_init(GPIO_NUM_0);
   xTaskCreate(vTask1, "vTask1", 1024*2, NULL, 4, NULL);
}
